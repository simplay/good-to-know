# Some Useful Commands

## Set Python Interpreter in CoC

Select an interpreter by invoking the following command: `:CocCommand python.setInterpreter`

## Install Pycharm on Ubuntu 22.04 List

1. Download Pycharm

2. Extract the downloaded package using the tar command and move the files to the /opt directory.

```
tar -zxvf pycharm-*.tar.gz
sudo mkdir /opt/pycharm/
sudo chmod 777 /opt/pycharm/
mv pycharm-*/* /opt/pycharm/
```

3. Link the executable to `/usr/bin directory` so that you can start PyCharm using the `pycharm` command from the terminal.

```
sudo ln -sf /opt/pycharm/bin/pycharm.sh /usr/bin/pycharm
```

4. Create a desktop entry to start PyCharm from the Activities menu.

```
sudo nano /usr/share/applications/pycharm.desktop
```

5. Paste the following content into the above file.

```
[Desktop Entry]
Version=1.0
Type=Application
Name=PyCharm Edition
Icon=/opt/pycharm/bin/pycharm.svg
Exec="/opt/pycharm/bin/pycharm.sh" %f
Comment=Python IDE for Professional Developers
Categories=Development;IDE;
Terminal=false
StartupWMClass=jetbrains-pycharm-ce
StartupNotify=true
```

## Fix Ubuntu bootloader with GRUB

**Problem**: "GRUB starts in command line after reboot"

### Solution

1. List all available devices in grub via `ls`. This will return 

```
proc, (hd0), (hd0,gpt1), (hd1), (hd1,gpt1), ... 
```

2. Find partition that contains `(hdX,gptY)/boot/grub, (hdX,gptY)/grub, (hdX,gptY)/efi/boot/grub OR (hdX,gptY)/efi/grub` (here, X and Y are placeholders): `ls (hdX,gptY)`.

3. Assuming that the correspnding partion was `(hdX,gptY)`, set the boot parameters accordingly (to direct to the grub subdirectory): `set prefix=(hdX,gptY)/grub OR set prefix=(hdX,gptY)/boot/grub`. 

Next invoke:

```
set root=(hdX,gptY)
insmod linux
insmod normal
normal
```

4. Reboot (via `reboot`) and then, start Ubuntu. Open a terminal and update the grub configuration: `sudo update-grub`


## Broken Update

Cause:

`sudo apt update && sudo apt upgrade -y`

Error Message:

```
dpkg: error processing package libglib2.0-0:i386 (--configure):
 dependency problems - leaving triggers unprocessed
dpkg: dependency problems prevent processing triggers for libglib2.0-0:i386:
 libglib2.0-0:i386 depends on libpcre3; however:
  Package libpcre3:i386 is not configured yet.
```

Fix:

```
sudo dpkg --configure -a
sudo apt --fix-broken install
```

## Git Submodules

+ In order to add a Git submodule, use the “git submodule add” command and specify the URL of the Git remote repository to be included as a submodule.

`git submodule add <remote_url> <destination_folder>`

E.g., `git submodule add https://github.com/project/project.git vendors`

+ To pull a Git submodule, use the “git submodule update” command with the “–init” and the “–recursive” options.

`git submodule update --init --recursive`

+ In order to update an existing Git submodule, you need to execute the “git submodule update” with the “–remote” and the “–merge” option.

`git submodule update --remote --merge`


## OpenVPN Server (Dockerized)

https://github.com/kylemanna/docker-openvpn

## List files / directories with their used space

```
du -h --max-depth=1 . | sort -h
```

## List all files that match a pattern in sorted order.

IN this example, we want to match all files that exhibit the extension `.dat`:

```
du *.dat -csh | sort -h
```

## Find Recursively the Number of Files 

```
find DIR_NAME -type f | wc -l
```

## Offline Installation of Packages

```
sudo apt-get -qq --print-uris install build-essential linux-headers-$(uname -r) | cut -d\' -f 2 > urls.txt
```

copy the `urls.txt` to a USB stick and move over to a computer with Internet Access. Download all files from `urls.txt`

`cat urls.txt | xargs wget` and save them in a folder called `deb` on your stick.

Go back to your Ubuntu machine, plug in the thumbdrive and open a Terminal

```
sudo cp /media/YOUR_USERNAME/THUMBDRIVE_NAME/deb/* /var/cache/apt/archives/
sudo apt-get install build-essential linux-headers-$(uname -r)
```

## Mount a USB stick

```
sudo fdisk -l # here, USB stick correspond to the partition /dev/sdb1
sudo mkdir /media/USB
sudo mount -t vfat /dev/sdb1 /media/USB -o uid=1000
```

### Options:

```
uid=1000
gid=1000
Utf8
dmask=027
fmask=137
```

### For NTFS Drives:

```
sudo mount -t ntfs-3g /dev/sdb1 /media/USB
```

### Unmounting

```
sudo umount /dev/sdb1
sudo umount /media/USB
```

## Copy SD Card to Images

```
sudo fdisk -l
```

```
Device         Boot   Start      End  Sectors  Size Id Type
/dev/mmcblk0p1 *       2048  2099199  2097152    1G  c W95 FAT32 (LBA)
/dev/mmcblk0p2      2099200 31116287 29017088 13.9G 83 Linux
```

```
sudo umount /dev/mmcblk0
sudo dd if=/dev/mmcblk0 of=/home/simplay/sd-card-copy.img bs=1M status=progress
```

## Copy Image to SD Card

```
sudo dd if=~/sd-card-copy.img of=/dev/mmcblk0 bs=1M status=progress
```


## Installing Docker on a Raspberry Pi 3

```
sudo apt-get update && sudo apt-get upgrade
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $USER
```

## Create and use temporary swap files

```
# Allocate space, location and name of the file
sudo fallocate -l 16G <dir>/<name-of-the-file> 

# Check if the swapfile is created
ls -lh <dir>/<name-of-the-file>

# Make it secure
sudo chmod 600 <dir>/<name-of-the-file>

# Enable the swap space
sudo mkswap <dir>/<name-of-the-file>
sudo swapon <dir>/<name-of-the-file>
sudo swapon -s
```

## Software Raid on Ubuntu Server 20.04

+ Select "Custom storage layout" when you reach the storage configuration step of the installer.

+ If the disks have existing partitions, click on each disk under AVAILABLE DEVICES and then select REFORMAT. This will (temporarily) wipe out the partitions.

+ Now select the 1st disk to add as "boot" disk (same menu that had REFORMAT in).

+ Do the same with the 2nd disk. (**Note** on March 2021: If this does not work for you in the newest installer of 20.04.2, just skip that part. In the end the 1st disk will have 4 partitions (bios, /boot, swap and /) and the 2nd just 3 (/boot, swap and /), which doesn't really matter. If you want to have the same number of partitions on each disk, go ahead and create a 1 MB partition on the 2nd disk, before doing the 3 others. This way, blocks will be sync'ed between the 2 disks. It's possible that there's a bug in how the bios_grub partitions are created in the installer when using multiple disks as this behaviour is not consistent with version 20.04.1 or the current Ubuntu Server docs as referenced here.)

+ You should now see two 1.000M bios_grub partitions created under USED DEVICES. These small partitions will be used by GRUB for booting the server. (**Note** on March 2021: the setup works with one bios_grub partition as well).
The trick to setup a softRAID array is to create partitions for /boot, swap and / on each disk, but WITHOUT formatting them (and as such, there won't be a mount point for now).
So go ahead and "Add GPT Partition" on the 1st disk, give it a 1G size and choose to leave it unformatted. Do the same for the 2nd disk. These will be the /boot partitions for the softRAID array. Under each disk on AVAILABLE DEVICES you will now see "partition 2".

+ Now we'll prepare the swap partitions. "Add GPT Partition" on the 1st disk, give it the same or half the size of your RAM (e.g. let's say 16G cause we have 16G of actual RAM - but it's really up to you to decide that) and choose to leave it unformatted. Do the same for the 2nd disk. Under each disk on AVAILABLE DEVICES you will now see "partition 3".

+ Now we'll prepare the / partitions. "Add GPT Partition" on the 1st disk, do not set a size (so it uses all available) and choose to leave it unformatted as with all the other partitions you created so far. Do the same for the 2nd disk. Under each disk on AVAILABLE DEVICES you will now see "partition 4".

+ Now click on "Create software RAID (md)" under AVAILABLE DEVICES. We'll create the first softRAID partition (md0) by selecting the two "partition 2" entries (one from each disk). Click "Save".

+ Repeat the process for md1 and select the two "partition 3" entries. Hit "Save".

+ Repeat the process for md2 and select the two "partition 4" entries. Hit "Save".

+ We now have 3 pairs of AVAILABLE DEVICES which will now format as the actual softRAID partitions. So select md0 and then "Add GPT Partition", format as EXT4 and mount on /boot.

+ Select md1 and then "Add GPT Partition", format as SWAP.

+ Select md2 and then "Add GPT Partition", format as EXT4 and mount on /.

+ All these mdX softRAID partitions will now appear under USED DEVICES and you are ready to proceed with Ubuntu's installation.

+ At the very bottom, you should now see "Done" enabled so hit it and proceed.

## Create new OpenVPN User

https://github.com/kylemanna/docker-openvpn

## Make a custom script globally available

Create a symlink of a target script in `/usr/local/bin/lazydocker`. 

E.g. `ln -s /home/simplay/.local/bin/lazydocker  /usr/local/bin/lazydocker`

## Run script on Startup

For services and the like you should use upstart. But for a user script these should be launched as session scripts by gnome! Have a look under `System > Preferences > Startup Applications`.

```
start on startup
task
exec /path/to/command
```

Save this in a `.conf` file in `/etc/init` (if you need it to run as root when the system boots up), or in `~/.config/upstart` (if you need it to run as your user when you log in).

### Via Crontab Entry

1. Running crontab -e will allow you to edit your cron.
2. Adding a line like this to it: `@reboot /path/to/script`


